# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'scanner_gui.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_IPSca(object):
    def setupUi(self, IPSca):
        IPSca.setObjectName("IPSca")
        IPSca.resize(506, 688)
        font = QtGui.QFont()
        font.setPointSize(10)
        IPSca.setFont(font)
        IPSca.setStyleSheet("selection-color: rgb(0, 0, 255);\n"
"selection-background-color: rgb(255, 255, 255);\n"
"alternate-background-color: rgb(255, 255, 255);\n"
"background-color: rgb(48, 48, 48);")
        self.scan_btn = QtWidgets.QPushButton(IPSca)
        self.scan_btn.setGeometry(QtCore.QRect(320, 330, 81, 23))
        font = QtGui.QFont()
        font.setFamily("MS Shell Dlg 2")
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.scan_btn.setFont(font)
        self.scan_btn.setStyleSheet("color: rgb(190, 0, 0);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
"border: none;\n"
"font: 75 14pt \"MS Shell Dlg 2\";\n"
"font-weight: bold;\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"stop: 0 #a6a6a6, stop: 0.08 #7f7f7f,\n"
"stop: 0.39999 #717171, stop: 0.4 #626262,\n"
"stop: 0.9 #4c4c4c, stop: 1 #333333);\n"
"}\n"
"\n"
"#bottomFrame {\n"
"border: none;\n"
"background: white;\n"
"")
        self.scan_btn.setObjectName("scan_btn")
        self.import_btn = QtWidgets.QToolButton(IPSca)
        self.import_btn.setGeometry(QtCore.QRect(140, 40, 31, 21))
        self.import_btn.setStyleSheet("color: rgb(190, 0, 0);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
"border: none;\n"
"font: 75 14pt \"MS Shell Dlg 2\";\n"
"font-weight: bold;\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"stop: 0 #a6a6a6, stop: 0.08 #7f7f7f,\n"
"stop: 0.39999 #717171, stop: 0.4 #626262,\n"
"stop: 0.9 #4c4c4c, stop: 1 #333333);\n"
"}\n"
"\n"
"#bottomFrame {\n"
"border: none;\n"
"background: white;\n"
"")
        self.import_btn.setObjectName("import_btn")
        self.targetsList = QtWidgets.QTextEdit(IPSca)
        self.targetsList.setGeometry(QtCore.QRect(20, 70, 151, 241))
        font = QtGui.QFont()
        font.setItalic(False)
        font.setUnderline(False)
        self.targetsList.setFont(font)
        self.targetsList.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.targetsList.setStyleSheet("color: rgb(115, 115, 115);\n"
"background-color: rgb(43, 43, 43);")
        self.targetsList.setObjectName("targetsList")
        self.terminal = QtWidgets.QTextBrowser(IPSca)
        self.terminal.setGeometry(QtCore.QRect(20, 410, 471, 181))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.terminal.setFont(font)
        self.terminal.setStyleSheet("background-color: rgb(43, 43, 43);")
        self.terminal.setObjectName("terminal")
        self.label = QtWidgets.QLabel(IPSca)
        self.label.setGeometry(QtCore.QRect(20, 40, 121, 21))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setStyleSheet("color: rgb(170, 0, 0);")
        self.label.setObjectName("label")
        self.threads_edit = QtWidgets.QLineEdit(IPSca)
        self.threads_edit.setGeometry(QtCore.QRect(450, 40, 41, 20))
        self.threads_edit.setStyleSheet("background-color: rgb(43, 43, 43);\n"
"selection-color: rgb(0, 0, 255);\n"
"alternate-background-color: rgb(0, 170, 0);\n"
"color: rgb(170, 0, 0);")
        self.threads_edit.setObjectName("threads_edit")
        self.label_2 = QtWidgets.QLabel(IPSca)
        self.label_2.setGeometry(QtCore.QRect(400, 40, 51, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color: rgb(170, 0, 0);")
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(IPSca)
        self.label_3.setGeometry(QtCore.QRect(360, 70, 91, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("color: rgb(170, 0, 0);")
        self.label_3.setObjectName("label_3")
        self.timeout_edit = QtWidgets.QLineEdit(IPSca)
        self.timeout_edit.setGeometry(QtCore.QRect(450, 70, 41, 20))
        self.timeout_edit.setStyleSheet("selection-color: rgb(0, 0, 255);\n"
"background-color: rgb(43, 43, 43);\n"
"color: rgb(170, 0, 0);")
        self.timeout_edit.setObjectName("timeout_edit")
        self.progressBar = QtWidgets.QProgressBar(IPSca)
        self.progressBar.setGeometry(QtCore.QRect(20, 600, 471, 21))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.progressBar.setFont(font)
        self.progressBar.setStyleSheet("QProgressBar {\n"
"border: 1px solid black;\n"
"text-align: top;\n"
"padding: 1px;\n"
"border-top-left-radius: 7px;\n"
"border-bottom-left-radius: 7px;\n"
"border-top-right-radius: 7px;\n"
"border-bottom-right-radius: 7px;\n"
"background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1,\n"
"stop: 0 #fff,\n"
"stop: 0.4999 #eee,\n"
"stop: 0.5 #ddd,\n"
"stop: 1 #eee );\n"
"width: 15px;\n"
"}\n"
"QProgressBar::chunk {\n"
"background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,\n"
"stop: 0 #0000ff,\n"
"stop: 1 #ff0000 );\n"
"border-top-left-radius: 7px;\n"
"border-bottom-left-radius: 7px;\n"
"border-top-right-radius: 7px;\n"
"border-bottom-right-radius: 7px;\n"
"border: 1px solid black;\n"
"}")
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(False)
        self.progressBar.setObjectName("progressBar")
        self.stop_btn = QtWidgets.QPushButton(IPSca)
        self.stop_btn.setGeometry(QtCore.QRect(410, 330, 81, 23))
        self.stop_btn.setStyleSheet("color: rgb(190, 0, 0);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
"border: none;\n"
"font: 75 14pt \"MS Shell Dlg 2\";\n"
"font-weight: bold;\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"stop: 0 #a6a6a6, stop: 0.08 #7f7f7f,\n"
"stop: 0.39999 #717171, stop: 0.4 #626262,\n"
"stop: 0.9 #4c4c4c, stop: 1 #333333);\n"
"}\n"
"\n"
"#bottomFrame {\n"
"border: none;\n"
"background: white;\n"
"")
        self.stop_btn.setObjectName("stop_btn")
        self.filter_line = QtWidgets.QLineEdit(IPSca)
        self.filter_line.setGeometry(QtCore.QRect(130, 369, 361, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.filter_line.setFont(font)
        self.filter_line.setStyleSheet("color: rgb(115, 115, 115);\n"
"border-color: rgb(170, 170, 255);\n"
"background-color: rgb(43, 43, 43);")
        self.filter_line.setText("")
        self.filter_line.setObjectName("filter_line")
        self.label_5 = QtWidgets.QLabel(IPSca)
        self.label_5.setGeometry(QtCore.QRect(20, 370, 101, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_5.setFont(font)
        self.label_5.setStyleSheet("color: rgb(170, 0, 0);")
        self.label_5.setObjectName("label_5")
        self.shuffle_btn = QtWidgets.QPushButton(IPSca)
        self.shuffle_btn.setGeometry(QtCore.QRect(20, 310, 151, 23))
        font = QtGui.QFont()
        font.setFamily("MS Shell Dlg 2")
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.shuffle_btn.setFont(font)
        self.shuffle_btn.setStyleSheet("color: rgb(190, 0, 0);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
"border: none;\n"
"font: 75 14pt \"MS Shell Dlg 2\";\n"
"font-weight: bold;\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"stop: 0 #a6a6a6, stop: 0.08 #7f7f7f,\n"
"stop: 0.39999 #717171, stop: 0.4 #626262,\n"
"stop: 0.9 #4c4c4c, stop: 1 #333333);\n"
"}\n"
"\n"
"#bottomFrame {\n"
"border: none;\n"
"background: white;\n"
"")
        self.shuffle_btn.setObjectName("shuffle_btn")
        self.iot_chbx = QtWidgets.QCheckBox(IPSca)
        self.iot_chbx.setGeometry(QtCore.QRect(410, 110, 81, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.iot_chbx.setFont(font)
        self.iot_chbx.setStyleSheet("color: rgb(170, 0, 0);")
        self.iot_chbx.setObjectName("iot_chbx")
        self.brute_chbx = QtWidgets.QCheckBox(IPSca)
        self.brute_chbx.setGeometry(QtCore.QRect(410, 140, 97, 25))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.brute_chbx.setFont(font)
        self.brute_chbx.setStyleSheet("color: rgb(170, 0, 0);")
        self.brute_chbx.setObjectName("brute_chbx")
        self.progress = QtWidgets.QLabel(IPSca)
        self.progress.setGeometry(QtCore.QRect(20, 630, 301, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.progress.setFont(font)
        self.progress.setStyleSheet("color: rgb(239, 225, 19);")
        self.progress.setText("")
        self.progress.setObjectName("progress")
        self.label_4 = QtWidgets.QLabel(IPSca)
        self.label_4.setGeometry(QtCore.QRect(280, 640, 61, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet("color: rgb(170, 0, 0);")
        self.label_4.setObjectName("label_4")
        self.format_combo = QtWidgets.QComboBox(IPSca)
        self.format_combo.setGeometry(QtCore.QRect(340, 640, 71, 31))
        self.format_combo.setStyleSheet("color: rgb(190, 0, 0);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
"border: none;\n"
"font: 75 14pt \"MS Shell Dlg 2\";\n"
"font-weight: bold;\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"stop: 0 #a6a6a6, stop: 0.08 #7f7f7f,\n"
"stop: 0.39999 #717171, stop: 0.4 #626262,\n"
"stop: 0.9 #4c4c4c, stop: 1 #333333);\n"
"\n"
"}\n"
"\n"
"#bottomFrame {\n"
"border: none;\n"
"background: white;\n"
"")
        self.format_combo.setObjectName("format_combo")
        self.format_combo.addItem("")
        self.format_combo.addItem("")
        self.format_combo.addItem("")
        self.submit_export = QtWidgets.QPushButton(IPSca)
        self.submit_export.setGeometry(QtCore.QRect(420, 640, 71, 31))
        self.submit_export.setStyleSheet("color: rgb(190, 0, 0);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
"border: none;\n"
"font: 75 14pt \"MS Shell Dlg 2\";\n"
"font-weight: bold;\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"stop: 0 #a6a6a6, stop: 0.08 #7f7f7f,\n"
"stop: 0.39999 #717171, stop: 0.4 #626262,\n"
"stop: 0.9 #4c4c4c, stop: 1 #333333);\n"
"}\n"
"\n"
"#bottomFrame {\n"
"border: none;\n"
"background: white;\n"
"")
        self.submit_export.setObjectName("submit_export")
        self.export_path = QtWidgets.QLabel(IPSca)
        self.export_path.setGeometry(QtCore.QRect(220, 680, 281, 16))
        self.export_path.setStyleSheet("color: rgb(170, 0, 0);")
        self.export_path.setText("")
        self.export_path.setObjectName("export_path")
        self.label_6 = QtWidgets.QLabel(IPSca)
        self.label_6.setGeometry(QtCore.QRect(410, 250, 51, 21))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setStyleSheet("color: rgb(170, 0, 0);")
        self.label_6.setObjectName("label_6")
        self.dead = QtWidgets.QLabel(IPSca)
        self.dead.setGeometry(QtCore.QRect(410, 271, 51, 20))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.dead.setFont(font)
        self.dead.setStyleSheet("color: rgb(170, 0, 0);")
        self.dead.setObjectName("dead")
        self.label_8 = QtWidgets.QLabel(IPSca)
        self.label_8.setGeometry(QtCore.QRect(360, 290, 111, 20))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color: rgb(170, 0, 0);")
        self.label_8.setObjectName("label_8")
        self.alive_label = QtWidgets.QLabel(IPSca)
        self.alive_label.setGeometry(QtCore.QRect(470, 250, 61, 21))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.alive_label.setFont(font)
        self.alive_label.setStyleSheet("color: rgb(255, 255, 255);\n"
"color: rgb(170, 170, 170);")
        self.alive_label.setObjectName("alive_label")
        self.dead_label = QtWidgets.QLabel(IPSca)
        self.dead_label.setGeometry(QtCore.QRect(470, 270, 51, 21))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.dead_label.setFont(font)
        self.dead_label.setObjectName("dead_label")
        self.successful_label = QtWidgets.QLabel(IPSca)
        self.successful_label.setGeometry(QtCore.QRect(470, 290, 47, 21))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.successful_label.setFont(font)
        self.successful_label.setObjectName("successful_label")
        self.label.setBuddy(self.import_btn)

        self.retranslateUi(IPSca)
        QtCore.QMetaObject.connectSlotsByName(IPSca)

    def retranslateUi(self, IPSca):
        _translate = QtCore.QCoreApplication.translate
        IPSca.setWindowTitle(_translate("IPSca", "IPSca"))
        self.scan_btn.setText(_translate("IPSca", "SCAN"))
        self.import_btn.setText(_translate("IPSca", "..."))
        self.targetsList.setHtml(_translate("IPSca", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Roboto\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:10pt;\">127.0.0.1:8080</span></p></body></html>"))
        self.label.setText(_translate("IPSca", "Targets (ip:port)"))
        self.threads_edit.setText(_translate("IPSca", "5"))
        self.label_2.setText(_translate("IPSca", "Threads"))
        self.label_3.setText(_translate("IPSca", "SocketTimeout"))
        self.timeout_edit.setText(_translate("IPSca", "1"))
        self.stop_btn.setText(_translate("IPSca", "STOP"))
        self.label_5.setText(_translate("IPSca", "Filter (optional):"))
        self.shuffle_btn.setText(_translate("IPSca", "Shuffle"))
        self.iot_chbx.setText(_translate("IPSca", "IoT only"))
        self.brute_chbx.setText(_translate("IPSca", "Bruteforce"))
        self.label_4.setText(_translate("IPSca", "Export:"))
        self.format_combo.setItemText(0, _translate("IPSca", "txt"))
        self.format_combo.setItemText(1, _translate("IPSca", "xml"))
        self.format_combo.setItemText(2, _translate("IPSca", "html"))
        self.submit_export.setText(_translate("IPSca", "OK"))
        self.label_6.setText(_translate("IPSca", "ALIVE:"))
        self.dead.setText(_translate("IPSca", "DEAD:"))
        self.label_8.setText(_translate("IPSca", "SUCCESSFUL:"))
        self.alive_label.setText(_translate("IPSca", "0"))
        self.dead_label.setText(_translate("IPSca", "<span style=\"color:#FF0000;\">0</span>"))
        self.successful_label.setText(_translate("IPSca", "<span style=\"color:#4cbb17;\">0</span>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    IPSca = QtWidgets.QDialog()
    ui = Ui_IPSca()
    ui.setupUi(IPSca)
    IPSca.show()
    sys.exit(app.exec_())

