from utils.tools import cut
from utils.paint import *
# logger = Logger()

class Analyser:

    def __init__(self, response):

        self.response = response
        self.data = {'headers': response.headers, 'title': cut(response.text, '<title>', '</title>')}
        self.device = ''
        self.vendor = ''
        self.authservice = ''
        self.authmethod = 'Server' if 'Server' in self.data['headers'] \
            else 'WWW-Authenticate' if 'WWW-Authenticate' in self.data['headers'] else ''
        self.authenticate = '' if 'WWW-Authenticate' not in self.data['headers'] else self.data['headers']['WWW-Authenticate']
        self.vulnerabilities = ['uc-httpd 1.0.0',  'P3P: CP=CAO PSA OUR', 'GoAhead+5ccc069c403ebaf9f0171e9517f40e41&'] #'thttpd/2.25b',
        self.devices ={'IPCAM': {'Hipcam': ['Basic realm="index.html"'],
                                    'Hikvision': ['App-webs/', 'DNVRS-Webs', 'DVRDVS-Webs', 'Hikvision-Webs', 'webserver'],
                                    'GoAhead': ['GoAhead-Webs', 'Digest realm="WIFICAM"', 'Basic realm="Internet Camera"', 'GoAhead-http'],
                                    'ActiveCam': ['ActiveCam'],
                                    'NETSurveillance': ['uc-httpd 1.0.0'],
                                    'BEWARD': ['N100 H.264 IP Camera'],
                                    'D-Link': ['DCS-', 'DI-'],
                                    'AXIS': ['AXIS'],
                                    'Foscam': ['IPCam Client'],
                                    'Netwave': ['Netwave IP Camera'],
                                    'Undefined': ['WebCam', 'Camera', 'DVR', 'NVR']},

                        'ROUTER': {'TP-LINK': ['TP-LINK', 'TL-WR'],
                                    'ZyXEL': ['ZyXEL'],
                                    'D-LINK': ['D-LINK'],
                                    'ASUS': ['RT-N', 'RT-G', 'RT-AC', 'RT-AX', 'RT-AC87U'],
                                    'CISCO': ['level_15 or view_access', 'level_15_access'],
                                    'DD-WRT': ['DD-WRT'],
                                    'MikroTik': ['Mikrotik']}}

        self.done = self.identifying_device()
        self.vulnerability = self.isVulnerability()


    def identifying_device(self):
        if len(self.authservice) == 0 and len(self.authmethod) == 0:
            return False
        for dev in self.devices:
            for vend in self.devices[dev]:
                if any([sign in self.data['headers'][self.authmethod] for sign in self.devices[dev][vend]]) \
                        or any([sign in self.data['title'] for sign in self.devices[dev][vend]]):
                    for sign in self.devices[dev][vend]:
                        if sign in self.data['headers'][self.authmethod] or sign in self.data['title']:
                            self.authservice = sign
                    self.device = dev
                    self.vendor = vend
                    return True
        return False

    def isVulnerability(self):
        for vul in self.vulnerabilities:
            if vul in self.data:
                return True
        return False

    def make_results(self):
        res = f'[{green(self.vendor)}][{blue(self.device)}]'
        if self.vulnerability:
            res += f'[{red("VULN")}]'
        return bold(res), self.device, self.vendor, self.authmethod, self.authservice, self.authenticate
